<?php

require __DIR__ . '/vendor/autoload.php';

use FacebookAds\Object\AdAccount;
use FacebookAds\Api;
use FacebookAds\Object\Business;
use FacebookAds\Http\Exception\ClientException;
use FacebookAds\Cursor;
use FacebookAds\Http\Exception\RequestException;

CONST APP_ID = '<APP_ID>';
CONST APP_SECRET = '<APP_SECRET>';
CONST BM_ID = '<BM_ID>';
CONST BM_TOKEN = '<BM_TOKEN>';

Cursor::setDefaultUseImplicitFetch(true);

$fields = ['account_id', 'name', 'account_status'];

Api::init(APP_ID, APP_SECRET, BM_TOKEN);
$api = Api::instance();
$business = new Business(BM_ID, null, $api);

$accounts_from_owner   = $business->getOwnedAdAccounts($fields, array(), false);  #Cuentas propias del BM
console_log($accounts_from_owner);
$accounts_from_clients = $business->getClientAdAccounts($fields, array(), false); #Cuentas compartidas
console_log($accounts_from_clients);

/**
 * Muestra en pantalla los costos o errores alintentar obtener los costos
 * @param  array $accounts
 */
function console_log($accounts) {

	foreach ($accounts as $account) {
	    # Evitar cuentas cerradas : 101: CLOSE
	    if ($account->account_status != 101 && $account->account_status != NULL) {
	        $data = [
	            'account_id' => $account->account_id,
	            'name'       => $account->name
	        ];

	        $cost = get_cost($data);
			if ($cost['Estado']) {
			    echo '<pre>';
			    print_r($cost);
			    echo '</pre>';
			} else {
				echo '</br>';
	           	print_r("<li>Cuenta <strong>" . $data['account_id'] . "</strong>: Error API, " . $cost['mensaje'] . '</li>');
	           	echo '</br>';
	        }
	    }
	}
}

/**
 * Obtiene costo de API Facebook ads
 * @param  array $cuenta
 * @return array
 */
function get_cost($cuenta) {

    $data  = ['Estado' => true, 'cuenta_id' => $cuenta['account_id'], 'name' => $cuenta['name'], 'Costo' => 0];

    try {
	    Api::init(APP_ID, APP_SECRET, BM_TOKEN); #Inicializar una nueva sesión e instanciar un objeto Api
	    $api = Api::instance();
	    $account = new AdAccount('act_' . $cuenta['account_id'], null, $api);
	    $params = array(
	        'level'      => 'account',
	        'fields'     => 'account_id,account_name,date_start,date_stop,spend',
	        'time_range' => array(
	            'since' => date('Y-m-d',strtotime("-1 days")), #fecha del dia de ayer
	            'until' => date('Y-m-d',strtotime("-1 days"))  #fecha del dia de ayer
	        )
	    );

	    $query = $account->getInsights(array(), $params);
	    if ($query->current() != false)
	        $data['Costo'] = $query->current()->getData()['spend'];

	    return $data;

    } catch (Exception $e) {
        return ['Estado' => false, 'mensaje' => $e->getMessage()];
    } catch (ClientException $http_exception) {
        return ['status' => false, 'mensaje' => $http_exception->getMessage()];
    } catch(RequestException $req_exception){
        return ['status' => false, 'mensaje' => $req_exception->getMessage()];
    }
}