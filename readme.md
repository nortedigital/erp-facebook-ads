# erp-facebook-ads

## Getting Started

- You must replace the APP_ID and APP_SECRET credentials in the index.php file.
- Replace constant manager with the array of BManagers in the index.php file

### Prerequisites

The apps requires PHP 5.4 or greater.

## Running the tests

Run from the terminal the index.php file

```
example ~ php index.php
```